using Godot;
using System;
using System.Text.RegularExpressions;

namespace UNO_DUO
{
    public class CardBehavior : Sprite
    {
        CardTypes _number;
        PackedScene sceneToSpawn;
        string _color;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            Label _valueLabel = GetNode<Label>("_valueLabel");
            _valueLabel.Text = "No";
        }

        // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _Process(float delta)
        {
            
        }

        public CardTypes VALUE
        {
            set
            {
                _number = value;

                //if (Regex.IsMatch(_number.ToString(), "^[1 - 9]$"))
                //{
                    Label _valueLabel = GetNode<Label>("_valueLabel");
                    _valueLabel.Text = _number.ToString();
                //}
                //else
                //{
                //    Label _valueLabel = GetNode<Label>("_valueLabel");
                //    _valueLabel.Text = "#";
                //}
            }
            get => _number;
        }
    }
}

