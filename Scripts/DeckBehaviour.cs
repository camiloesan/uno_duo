using Godot;
using System;

namespace UNO_DUO
{
    public class DeckBehaviour : Sprite
    {
        private PackedScene leftCard;
        private PackedScene rightCard;


        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            // Load the scene to spawn
            rightCard = (PackedScene)ResourceLoader.Load("res://Scenes/Card.tscn");
            leftCard = (PackedScene)ResourceLoader.Load("res://Scenes/Card.tscn");

            // Spawn the scene to the left of the deck
            Node spawnedRightCard = rightCard.Instance();
            Node spawnedLeftCard = rightCard.Instance();

            CardBehavior rightCardBehavior = (CardBehavior)spawnedRightCard;
            rightCardBehavior.Position = new Vector2(-1000, 0);
            AddChild(rightCardBehavior);

            CardBehavior leftCardBehavior = (CardBehavior)spawnedLeftCard;
            leftCardBehavior.Position = new Vector2(-2000, 0);
            AddChild(leftCardBehavior);

            // Assign card values
            Random randomGenerator = new Random();
            CardTypes[] cardValues = (CardTypes[])Enum.GetValues(typeof(CardTypes));

            int randomIndex = randomGenerator.Next(0, cardValues.Length);
            CardTypes rightValue = cardValues[randomIndex];
            rightCardBehavior.VALUE = rightValue;

            randomIndex = randomGenerator.Next(0, cardValues.Length);
            CardTypes leftValue = cardValues[randomIndex];
            leftCardBehavior.VALUE = leftValue;
        }

        // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _Process(float delta)
        {

        }
    }
}
