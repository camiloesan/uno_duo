using Godot;
using System;

namespace Scripts
{
    public class Launcher : Control
    {
        // Declare member variables here. Examples:
        // private int a = 2;
        // private string b = "text";

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            
        }

        private void _on_BtnNewAccount_pressed() 
        {
            GetTree().ChangeScene("res://Scenes/NewAccount.tscn");
        }

        private void _on_BtnLogin_pressed() 
        {
            GetTree().ChangeScene("res://Scenes/Login.tscn");
        }

        private void _on_BtnJoinAsGuest_pressed()
        {
            GetTree().ChangeScene("res://Scenes/JoinAsGuest.tscn");
        }

        private void _on_BtnEnglish_pressed()
        {
            TranslationServer.SetLocale("en_US");
        }

        private void _on_BtnSpanish_pressed()
        {
            TranslationServer.SetLocale("es_MX");
        }

        private void _on_BtnJapanese_pressed()
        {
            TranslationServer.SetLocale("ja_JP");
        }

        private void _on_BtnFrench_pressed()
        {
            TranslationServer.SetLocale("fr_FR");
        }

        //  // Called every frame. 'delta' is the elapsed time since the previous frame.
        //  public override void _Process(float delta)
        //  {
        //      
        //  }
    }
}

