using Godot;
using System;

namespace Scripts
{
    public class Login : Control
    {
        // Declare member variables here. Examples:
        // private int a = 2;
        // private string b = "text";

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {

        }

        private void _on_BtnContinue_pressed()
        {
            //ToDo validation
            GetTree().ChangeScene("res://Scenes/MainMenu.tscn");
        }

        private void _on_BtnCancel_pressed() 
        {
            GetTree().ChangeScene("res://Scenes/Launcher.tscn");
        }

        //  // Called every frame. 'delta' is the elapsed time since the previous frame.
        //  public override void _Process(float delta)
        //  {
        //      
        //  }
    }

}
