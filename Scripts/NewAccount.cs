using Godot;
using System;
using System.Security.AccessControl;
using System.Text.RegularExpressions;

namespace Scripts
{
    public class NewAccount : Control
    {
        // Declare member variables here. Examples:
        // private int a = 2;
        // private string b = "text";

        LineEdit lineEditUsername;
        LineEdit lineEditEmail;
        LineEdit lineEditPassword;
        LineEdit lineEditConfirmPassword;
        AcceptDialog alertDialog;
        AcceptDialog alertDialogConfirmation;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            lineEditUsername = GetNode("VBoxContainer/LineEditUsername") as LineEdit;
            lineEditEmail = GetNode("VBoxContainer/LineEditEmail") as LineEdit;
            lineEditPassword = GetNode("VBoxContainer/LineEditPassword") as LineEdit;
            lineEditConfirmPassword = GetNode("VBoxContainer/LineEditConfirmPassword") as LineEdit;
            alertDialog = GetNode("AlertDialog") as AcceptDialog;
            alertDialogConfirmation = GetNode("AlertDialogConfirmation") as AcceptDialog;
        }

        private void _on_BtnCancel_pressed()
        {
            GetTree().ChangeScene("res://Scenes/Launcher.tscn");
        }

        private void _on_BtnContinue_pressed()
        {
            if (AreFieldsValid())
            {
                alertDialogConfirmation.DialogText = "AlertNewAccountSuccess";
                alertDialogConfirmation.Visible = true;
            }
        }

        private bool AreFieldsValid()
        {
            string username = lineEditUsername.Text;
            string email = lineEditEmail.Text;
            string password = lineEditPassword.Text;
            string passwordConfirmation = lineEditConfirmPassword.Text;

            if (AreFieldsEmpty())
            {
                alertDialog.DialogText = "AlertEmptyFields";
                alertDialog.Visible = true;
                return false;
            }
            else if (!IsUsernameValid(username))
            {
                alertDialog.DialogText = "AlertInvalidUsername";
                alertDialog.Visible = true;
                return false;
            }
            else if (!IsEmailValid(email))
            {
                alertDialog.DialogText = "AlertInvalidEmail";
                alertDialog.Visible = true;
                return false;
            }
            else if (!IsPasswordSecure(password))
            {
                alertDialog.DialogText = "AlertPasswordNotSecure";
                alertDialog.Visible = true;
                return false;
            }
            else if (!IsSamePassword(password, passwordConfirmation))
            {
                alertDialog.DialogText = "AlertPasswordDoesNotMatch";
                alertDialog.Visible = true;
                return false;
            }
            return true;
        }

        private bool AreFieldsEmpty()
        {
            string username = lineEditUsername.Text;
            string email = lineEditEmail.Text;
            string password = lineEditPassword.Text;
            string passwordConfirmation = lineEditConfirmPassword.Text;

            if (string.IsNullOrEmpty(username)
                || string.IsNullOrEmpty(email)
                || string.IsNullOrEmpty(password)
                || string.IsNullOrEmpty(passwordConfirmation))
            {
                return true;
            }
            return false;
        }

        private bool IsUsernameValid(string username)
        {
            Regex alphanumericExpression = new Regex("^[a-zA-Z0-9]*$");
            
            if (alphanumericExpression.IsMatch(username))
            {
                return true;
            }
            return false;
        }

        private bool IsEmailValid(string email)
        {
            Regex emailExpression = new Regex("^\\S+@\\S+\\.\\S+$");

            if (emailExpression.IsMatch(email))
            {
                return true;
            }
            return false;
        }

        private const int MinCharactersPassword = 8;
        private bool IsPasswordSecure(string password)
        {
            if (password.Length >= MinCharactersPassword)
            {
                return true;
            }
            return false;
        }

        private bool IsSamePassword(string password, string passwordConfirmation)
        {
            if (passwordConfirmation.Equals(password))
            {
                return true;
            }
            return false;
        }

        private void _on_AlertDialogConfirmation_confirmed()
        {
            GetTree().ChangeScene("res://Scenes/Launcher.tscn");
        }

        //  // Called every frame. 'delta' is the elapsed time since the previous frame.
        //  public override void _Process(float delta)
        //  {
        //      
        //  }
    }
}

